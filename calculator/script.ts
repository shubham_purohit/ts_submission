let result  = (<HTMLInputElement>document.getElementById('result'));

//to display value 
function display(val: string) {
    console.log(val);
    result.value = result.value + val;
}

//to evaluate the  digits 
function solve() {
    let x: String = result.value;

    // create tokens based on delimeter(operators)
    //  array of tokens
    var numbers: string[] = x.split(/[\/, +, \-, *]+/);
    var oper: string[] = x.split(/[0-9\s]/g);

    let res: unknown= 0;
    let idx: number = 0,
        flag: number = 1,
        ind: number = 0;
    var error: number = 0;

    // handling negative numbers
    if (oper[0] == "-") {
        numbers[1] = "-" + numbers[1];
        idx++;
        ind++;
        res = parseFloat(numbers[1]);
        flag = 0;
    }

    // loop to search for operators and subsequent calculation
    for (; ind < oper.length; ind++) {
        if (oper[ind] != "") {
            switch (oper[ind]) {
                case '+':
                    if (flag) {
                        res = parseFloat(numbers[idx]) + parseFloat(numbers[++idx]);
                        flag = 0;
                    } else
                        res = res as number + parseInt(numbers[++idx]);
                    break;
                case '-':
                    if (flag) {
                        res = parseFloat(numbers[idx]) - parseFloat(numbers[++idx]);
                        flag = 0;
                    } else
                        res = res as number- parseInt(numbers[++idx]);

                    break;
                case '*':
                    if (flag) {
                        res = parseFloat(numbers[idx]) * parseFloat(numbers[++idx]);
                        flag = 0;
                    } else
                        res = res as number * parseInt(numbers[++idx]);

                    break;
                case '/':
                    if (flag) {
                        res = parseFloat(numbers[idx]) / parseFloat(numbers[++idx]);
                        flag = 0;
                    } else
                        res = res as number / parseInt(numbers[++idx]);

                    break;
                default:
                    if (oper[ind].match(/[^*/+\-]*/g)) {
                        alert("Invalid Expression");
                        error = 1;
                    }
            }
            if (error)
                break;
        }
    }

    console.log(numbers);
    console.log(oper);
    console.log(res);

    if (!error) {
        result.value = res as string;
    }
}

//clear the display on press of c button
function clr() {
  result.value = "";
}