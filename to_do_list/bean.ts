/*   Bean class to create the list object  */

class ToDoList
{
    listTitle: string;
    listItems: Array<string>;

    constructor(listTitle:string, listItems: Array<string>)
    {
        this.listTitle = listTitle; 
        this.listItems = listItems;
    }
}