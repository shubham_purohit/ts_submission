function editItemHandler(element: HTMLImageElement): void
{
    // set the default input field
    inputField.value = element.getAttribute("item");
    inputField.placeholder = "Add-item"; 

    // update the global variables
    editList = element.getAttribute("list");
    editedItem = element.getAttribute("item");

    // configure the doen button behaviour
    doneMode = edit_item;
    display(add_mode);
}

function addItemHandler(element: HTMLImageElement): void
{
    // set the default input field
    inputField.value = "";
    inputField.placeholder = "Add-item";

    // update the global variables
    editList = element.getAttribute("list");

    // configure the doen button behaviour 
    doneMode = add_item;
    display(add_mode);
}

function deleteItemHandler(element: HTMLImageElement): void
{
    // update the global variables
    editList = element.getAttribute("list");
    editedItem = element.getAttribute("item");

    deleteItemLocalStorage();

    // refresh the view page
    refreshLoad();
    loadLists(todoListsResponse);
    display(view_mode);
}

function deleteListHandler(element: HTMLImageElement): void
{
    // update the global variables
    editList = element.getAttribute("list");
    deleteListLocalStorage();

    // refresh the view page
    refreshLoad();
    loadLists(todoListsResponse);
    display(view_mode);
}
