
var doneButton: HTMLElement = document.getElementById("done_button");
var inputField = (<HTMLInputElement>document.getElementById("title_input"));

doneButton.addEventListener("click", function () {
    if (todoListsResponse == null || todoListsResponse == undefined) {
        refreshLoad();
    }

    updateLocalStorage(inputField.value)

    // refresh view page
    loadLists(todoListsResponse);
    display(view_mode)
});

