
// interface array_object
// {
//     [key : string] : (string | string[]);
// }

// base layouts
var addNew: HTMLElement = document.getElementById("add_new_button");
var addLayout: HTMLElement = document.getElementById("add_layout");
var viewLayout: HTMLElement = document.getElementById("view_layout"); 
var todoListsResponse: Array<ToDoList>;

// referesh and check the local storage
refreshLoad();
loadLists(todoListsResponse);

// adding new list handling
addNew.addEventListener("click",function()
{
    // toggle the display;
    display(add_mode);
    inputField.value = ""
    inputField.placeholder = "List Title";
    // setting mode for done button
    doneMode = add_lists;
    // create_addList_display();
});

// common lists loading on main page function
function loadLists(todo_lists_response : Array<ToDoList>): void{
    emptySection();
    if(todo_lists_response == null)
    {
        todo_lists_response = JSON.parse(localStorage.getItem("todo_lists"));
    }
    createLists(todo_lists_response);
    display(view_mode);
}

