/*   Bean class to create the list object  */
class ToDoList {
    constructor(listTitle, listItems) {
        this.listTitle = listTitle;
        this.listItems = listItems;
    }
}
//# sourceMappingURL=bean.js.map