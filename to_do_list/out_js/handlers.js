function editItemHandler(element) {
    // set the default input field
    input_field.value = element.getAttribute("item");
    input_field.placeholder = "Add-item";
    // update the global variables
    editList = element.getAttribute("list");
    editedItem = element.getAttribute("item");
    // configure the doen button behaviour
    done_mode = edit_item;
    display(add_mode);
}
function addItemHandler(element) {
    // set the default input field
    input_field.value = "";
    input_field.placeholder = "Add-item";
    // update the global variables
    editList = element.getAttribute("list");
    // configure the doen button behaviour 
    done_mode = add_item;
    display(add_mode);
}
function deleteItemHandler(element) {
    // update the global variables
    editList = element.getAttribute("list");
    editedItem = element.getAttribute("item");
    deleteItemLocalStorage();
    // refresh the view page
    refreshLoad();
    loadLists(todo_lists_response);
    display(view_mode);
}
function deleteListHandler(element) {
    // update the global variables
    editList = element.getAttribute("list");
    deleteListLocalStorage();
    // refresh the view page
    refreshLoad();
    loadLists(todo_lists_response);
    display(view_mode);
}
//# sourceMappingURL=handlers.js.map