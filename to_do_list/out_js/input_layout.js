var done_button = document.getElementById("done_button");
var input_field = document.getElementById("title_input");
done_button.addEventListener("click", function () {
    if (todo_lists_response == null || todo_lists_response == undefined) {
        refreshLoad();
    }
    updateLocalStorage(input_field.value);
    // refresh view page
    loadLists(todo_lists_response);
    display(view_mode);
});
//# sourceMappingURL=input_layout.js.map