// interface array_object
// {
//     [key : string] : (string | string[]);
// }
// base layouts
var add_new = document.getElementById("add_new_button");
var add_layout = document.getElementById("add_layout");
var view_layout = document.getElementById("view_layout");
var todo_lists_response;
// referesh and check the local storage
refreshLoad();
loadLists(todo_lists_response);
// adding new list handling
add_new.addEventListener("click", function () {
    // toggle the display;
    display(add_mode);
    input_field.value = "";
    input_field.placeholder = "List Title";
    // setting mode for done button
    done_mode = add_lists;
    // create_addList_display();
});
// common lists loading on main page function
function loadLists(todo_lists_response) {
    emptySection();
    if (todo_lists_response == null) {
        todo_lists_response = JSON.parse(localStorage.getItem("todo_lists"));
    }
    createLists(todo_lists_response);
    display(view_mode);
}
//# sourceMappingURL=main.js.map