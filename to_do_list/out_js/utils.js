// handle multiple lists 
// create individual list div and populate items in it
function createLists(todo_lists_response) {
    console.log(todo_lists_response);
    // each individual list in the local storage
    for (var list of todo_lists_response) {
        // main list container(div)
        var container = document.createElement("div");
        container.setAttribute("class", "container");
        container.style.width = "150px";
        container.style.height = "200px";
        container.style.display = "inline-grid";
        container.style.textAlign = "center";
        container.style.margin = "10px";
        container.style.overflow = "scroll";
        container.style.borderRadius = "10px";
        container.style.backgroundColor = "white";
        // list title
        var title = document.createElement("p");
        title.style.fontSize = "large";
        title.style.fontFamily = "fantasy";
        title.style.fontWeight = "bolder";
        title.style.margin = "10px";
        title.innerHTML = list.listTitle;
        container.appendChild(title);
        // each item of each list
        for (var item of list.listItems) {
            // eah row
            var item_container = document.createElement("div");
            item_container.style.width = "100%";
            item_container.style.textAlign = "left";
            item_container.style.margin = "5px";
            // list item
            var para = document.createElement("p");
            para.innerHTML = item;
            para.setAttribute("class", "list_items");
            item_container.appendChild(para);
            // edit button
            var edit_item = new Image();
            edit_item.src = "images/edit_icon.png";
            edit_item.setAttribute("class", "list_items");
            edit_item.style.width = "15px";
            edit_item.style.height = "15px";
            edit_item.style.display = "inline-block";
            edit_item.style.marginRight = "5px";
            edit_item.setAttribute("list", list.listTitle);
            edit_item.setAttribute("item", item);
            edit_item.addEventListener("click", function (event) {
                if (event && event.currentTarget) {
                    editItemHandler(event.currentTarget);
                }
            });
            item_container.appendChild(edit_item);
            // delete button
            var delete_item = new Image();
            delete_item.src = "images/delete_icon.png";
            delete_item.setAttribute("class", "list_items");
            delete_item.style.width = "15px";
            delete_item.style.height = "15px";
            delete_item.style.display = "inline-block";
            delete_item.style.marginRight = "5px";
            delete_item.setAttribute("list", list.listTitle);
            delete_item.setAttribute("item", item);
            delete_item.addEventListener("click", function (event) {
                if (event && event.currentTarget) {
                    deleteItemHandler(event.currentTarget);
                }
            });
            item_container.appendChild(delete_item);
            container.appendChild(item_container);
        }
        // bottom button fro add items and delete list
        var footer_container = document.createElement("div");
        footer_container.style.width = "100%";
        footer_container.style.textAlign = "center";
        footer_container.style.display = "inline-block";
        footer_container.style.margin = "5px";
        var add_items = new Image();
        add_items.src = "images/add_to_list.png";
        add_items.style.width = "15px";
        add_items.style.height = "15px";
        add_items.style.margin = "10px";
        add_items.style.display = "inline-block";
        add_items.setAttribute("list", list.listTitle);
        add_items.addEventListener("click", function (event) {
            if (event && event.currentTarget) {
                addItemHandler(event.currentTarget);
            }
        });
        var delete_list = new Image();
        delete_list.src = "images/delete_icon.png";
        delete_list.style.width = "15px";
        delete_list.style.height = "15px";
        delete_list.style.margin = "10px";
        delete_list.style.display = "inline-block";
        delete_list.setAttribute("list", list.listTitle);
        delete_list.addEventListener("click", function (event) {
            if (event && event.currentTarget) {
                deleteListHandler(event.currentTarget);
            }
        });
        footer_container.appendChild(add_items);
        footer_container.appendChild(delete_list);
        container.appendChild(footer_container);
        view_layout.appendChild(container);
    }
}
// dispplay toggle
function display(mode) {
    switch (mode) {
        case 1:
            view_layout.style.display = "block";
            add_layout.style.display = "none";
            break;
        case 2:
            view_layout.style.display = "none";
            add_layout.style.display = "block";
            break;
    }
}
// refresh loading the lists
function refreshLoad() {
    // fetch from the local storage
    todo_lists_response = JSON.parse(localStorage.getItem("todo_lists"));
    if (todo_lists_response == undefined || todo_lists_response == null) {
        var todo_lists_response = [];
        localStorage.setItem("todo_lists", JSON.stringify(todo_lists_response));
    }
}
// empty the div fro fresh display
function emptySection() {
    while (view_layout.firstChild) {
        view_layout.removeChild(view_layout.firstChild);
    }
}
// common function to update local storage
function updateLocalStorage(input_value) {
    todo_lists_response = JSON.parse(localStorage.getItem("todo_lists"));
    switch (done_mode) {
        case add_lists:
            addListsLocalStorage(input_value);
            break;
        case add_item:
            addItemLocalStorage(input_value);
            break;
        case edit_item:
            editItemLocalStorage(input_value);
            break;
    }
}
// add lists to the local storage
function addListsLocalStorage(input_value) {
    if (todo_lists_response != null && todo_lists_response != undefined && validate(input_value)) {
        const list_Object = new ToDoList(input_value, []);
        todo_lists_response.push(list_Object);
        localStorage.setItem("todo_lists", JSON.stringify(todo_lists_response));
    }
}
// add item in the lists and update local storage
function addItemLocalStorage(input_value) {
    if (todo_lists_response != null && todo_lists_response != undefined && validate(input_value)) {
        for (var list of todo_lists_response) {
            if (list.listTitle == editList) {
                list.listItems.push(input_value);
                break;
            }
        }
        localStorage.setItem("todo_lists", JSON.stringify(todo_lists_response));
    }
}
// eidt item in the lists and update local storage
function editItemLocalStorage(input_value) {
    if (todo_lists_response != null && todo_lists_response != undefined && validate(input_value)) {
        var list = searchItem(todo_lists_response);
        var index = list.listItems.indexOf(editedItem);
        if (list != null) {
            list.listItems[index] = input_value;
        }
        localStorage.setItem("todo_lists", JSON.stringify(todo_lists_response));
    }
}
// delete item in the lists and updsate local storage
function deleteItemLocalStorage() {
    todo_lists_response = JSON.parse(localStorage.getItem("todo_lists"));
    if (todo_lists_response != null && todo_lists_response != undefined) {
        var list = searchItem(todo_lists_response);
        if (list != null) {
            list.listItems.splice(list.listItems.indexOf(editedItem), 1);
        }
        localStorage.setItem("todo_lists", JSON.stringify(todo_lists_response));
    }
}
// delete list from the local storage
function deleteListLocalStorage() {
    todo_lists_response = JSON.parse(localStorage.getItem("todo_lists"));
    if (todo_lists_response != null && todo_lists_response != undefined) {
        for (var list of todo_lists_response) {
            // search for particular list and delete it
            if (list.listTitle == editList) {
                todo_lists_response.splice(todo_lists_response.indexOf(list), 1);
                break;
            }
        }
        localStorage.setItem("todo_lists", JSON.stringify(todo_lists_response));
    }
}
// empty input validation
function validate(input_value) {
    if (!input_value) {
        alert("Field Empty!");
        return false;
    }
    return true;
}
function searchItem(todo_lists_response) {
    for (var list of todo_lists_response) {
        // search for particular list
        if (list.listTitle == editList) {
            // search for particular item in list
            for (var index in list.listItems) {
                if (list.listItems[index] == editedItem) {
                    return list;
                }
            }
            break;
        }
    }
}
//# sourceMappingURL=utils.js.map