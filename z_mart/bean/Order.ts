class Order
{
    product:Product;
    productID:string;
    quantity:number;
    totalPrice:number;

    constructor(product:Product,productID:string,quantity:number,totalPrice:number)
    {
        this.product = product;
        this.productID = productID;
        this.quantity = quantity;
        this.totalPrice = totalPrice
    }
}