class Product{
    
    id: string;
    name: string;
    category: string;
    gender: string;
    color: string;
    size: string;
    price: number;
    stock: number;
    material: string;
    brand: string;
    description: string;
    image: string;
   
    constructor(id:string,name:string,category:string,gender:string,color:string,
        size:string,price:number,stock:number,material:string, brand:string,description:string,
        image:string)
    {
        this.id = id;
        this.name = name;
        this.category = category;
        this.gender = gender;
        this.color = color;
        this.size = size;
        this.price = price;
        this.material = material;
        this.brand = brand;
        this.description = description;
        this.image = image;
    }    
}