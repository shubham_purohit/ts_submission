class User
{
    f_name: string;
    s_name: string;
    mobile: string;
    password: string;
    orders:Array<Order>;
    cart:Array<Order>;

    constructor(f_name:string,s_name:string,mobile:string,password:string,
        orders:Array<Order>,cart:Array<Order>)
    {
        this.f_name = f_name;
        this.s_name = s_name;
        this.mobile = mobile;
        this.password = password;
        this.orders = orders;
        this.cart = cart;
    }
}