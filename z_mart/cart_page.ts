let cartListLayout: HTMLElement = document.getElementById("cart_list_div") as HTMLElement;
let totalQuantityDis: HTMLElement = document.getElementById("total_quantity") as HTMLElement;
let totalPriceDis: HTMLElement = document.getElementById("total_price") as HTMLElement;

let cartItemHeading: HTMLElement = document.createElement("div") as HTMLElement;
cartItemHeading.innerHTML = "Cart Items"
cartItemHeading.setAttribute("class", "cart_heading");

let cartPlaceOrderButton: HTMLElement = document.createElement("h1") as HTMLElement;
cartPlaceOrderButton.setAttribute("class", "place_order_button");
cartPlaceOrderButton.innerHTML = "Place Order";

// global items in cart variabl
let orderedCart: Array<Order> = JSON.parse(localStorage.getItem("logged_user")).cart;
let users: User[] = JSON.parse(localStorage.getItem("registered_users"));

let actualPrice: number;
let quantity: number;
let totalPrice: number = 0;
let totalQuantity: number = 0;

function cartLayoutInflator(): void {
    updateUserCart();
    emptyCartDisplay();
    updateTotalPrice();
    if (orderedCart.length > 0) {
        cartListLayout.style.backgroundImage = "none";
        cartListLayout.appendChild(cartItemHeading);
        for (var order of orderedCart) {
            createCartCard(order);
        }
    }
    else{
        emptyCartDisplay();
        cartListLayout.style.backgroundImage = "url('images/empty_cart.png')";
    }
   
    if (orderedCart.length > 0){
        cartListLayout.appendChild(cartPlaceOrderButton);
    }
}

// card for each product in the cart
function createCartCard(order: Order): void {
    let cartCard: HTMLElement = document.createElement("div") as HTMLElement;
    let infoContainer: HTMLElement = document.createElement("div") as HTMLElement;
    let imageContainer: HTMLElement = document.createElement("div") as HTMLElement;
    let cartImage: HTMLImageElement = document.createElement("img") as HTMLImageElement;
    let cartName: HTMLElement = document.createElement("h4") as HTMLElement;
    let cartPrice: HTMLElement = document.createElement("p") as HTMLElement;
    let cartSize: HTMLElement = document.createElement("p") as HTMLElement;
    let quantityDiv :HTMLDivElement = document.createElement("div") as HTMLDivElement;
    let cartQuantity: HTMLInputElement = document.createElement("input") as HTMLInputElement;
    let add_button:HTMLImageElement = document.createElement("img") as HTMLImageElement;
    let minus_button:HTMLImageElement = document.createElement("img") as HTMLImageElement;
    let cartItemDelete: HTMLElement = document.createElement("p") as HTMLElement;

    cartCard.setAttribute("id", "cart_card");
    imageContainer.setAttribute("id", "image_container");
    infoContainer.setAttribute("id", "info_container");
    add_button.setAttribute("class", "add");
    minus_button.setAttribute("class", "minus");

    cartImage.src = order.product.image;
    cartName.innerHTML = order.product.name;
    cartPrice.innerHTML = "Special Price: Rs: " + order.totalPrice + "/-";
    cartSize.innerHTML = "Size: " + order.product.size;
    cartQuantity.innerHTML = "Qnty."
    cartQuantity.value = <unknown>order.quantity as string;
    cartQuantity.setAttribute("type", "number");
    cartQuantity.setAttribute("productID", order.product.id);
    cartQuantity.addEventListener("change", function (event) {
        handleQuantity(add_button,event.currentTarget as HTMLInputElement);
    });

    add_button.src = "images/add_icon.png";
    add_button.addEventListener("click",function(event){
        handleQuantity(event.currentTarget as HTMLElement,cartQuantity);
        
    });
    minus_button.addEventListener("click",function(event){
        handleQuantity(event.currentTarget as HTMLElement,cartQuantity);
        
    });

    minus_button.src = "images/minus_icon.png";
    minus_button.style.width = "22px"
    minus_button.style.height = "22px"
    quantityDiv.appendChild(add_button);
    quantityDiv.append(cartQuantity);
    quantityDiv.appendChild(minus_button);

    cartItemDelete.innerHTML = "REMOVE";
    cartItemDelete.setAttribute("class", "remove_cart_item");
    cartItemDelete.setAttribute("productId", order.productID);
    cartItemDelete.addEventListener('click', function (event) {
        deleteCartItem(event.currentTarget as HTMLElement);
    });

    infoContainer.appendChild(cartName);
    infoContainer.appendChild(cartPrice);
    infoContainer.appendChild(cartSize);
    // infoContainer.appendChild(minus_button);
    // infoContainer.appendChild(cartQuantity);
    // infoContainer.appendChild(add_button);
    infoContainer.appendChild(quantityDiv); 
    infoContainer.appendChild(cartItemDelete);
    imageContainer.appendChild(cartImage);
    cartCard.appendChild(imageContainer);
    cartCard.appendChild(infoContainer);
    cartListLayout.appendChild(cartCard);
}

// handle the quantity change
function handleQuantity(buttonElement: HTMLElement,inputElement:HTMLInputElement): void {
    // avoid negative entry

    if(buttonElement.getAttribute("class") == "add")
    {
        inputElement.value = <unknown>((<unknown>inputElement.value as number) *1 + 1) as string;
    }
    else
    {
        inputElement.value = <unknown>((<unknown>inputElement.value as number) *1 - 1) as string; 
    }

    if (<unknown>inputElement.value as number < 1) {
        inputElement.value = "1";
    }

    updateIndividualPrice(inputElement);
    // updateTotalPrice();
    cartLayoutInflator();
}

// delete cart item
function deleteCartItem(element: HTMLElement): void {
    let cartItem: Order;
    // fetch the clicked item
    cartItem = getCartProduct(orderedCart, element.getAttribute("productID"));
    let index: number = orderedCart.indexOf(cartItem);
    orderedCart.splice(index, 1);
    cartLayoutInflator();
}

// update the item price after quantity change
function updateIndividualPrice(element: HTMLInputElement): void {
    let cartItem: Order;
    // fetch the clicked item
    cartItem = getCartProduct(orderedCart, element.getAttribute("productID"));
    let index: number = orderedCart.indexOf(cartItem);

    actualPrice = (cartItem.product as any).price as number;

    orderedCart[index].quantity = <unknown>element.value as number;

    quantity = orderedCart[index].quantity;
    orderedCart[index].totalPrice = actualPrice * quantity;
}

// update total price after quantity change
function updateTotalPrice(): void {
    totalQuantity = totalPrice = 0;

    totalPriceDis.innerHTML = " ";
    totalQuantityDis.innerHTML = " ";

    for (var cartItem of orderedCart) {
        totalPrice = (totalPrice + (1 * cartItem.totalPrice));
        totalQuantity = (totalQuantity + (1 * cartItem.quantity));
        console.log(totalPrice);
        console.log(totalQuantity);
    }

    console.log(totalPrice);
    console.log(totalQuantity);

    totalQuantityDis.innerHTML = "Total Quantity:  " + <unknown>totalQuantity as string;
    totalPriceDis.innerHTML = "Total Price:  Rs " + <unknown>totalPrice as string + "/-";
}

// fetch the  product from the cart
function getCartProduct(orderCart: Array<Order>, productID: string): Order {
    for (var order of orderCart) {
        if (order.productID == productID) {
            return order;
        }
    }
    return null;
}

// refresh the cart items list display
function emptyCartDisplay(): void {
    while (cartListLayout.firstChild) {
        cartListLayout.removeChild(cartListLayout.firstChild);
    }
}

// id already in cart or not
function inCart(cartOrder: Array<Order>, order: Order): boolean {
    for (var item of cartOrder) {
        if (item.productID == order.productID)
            return true;
    }
    return false;
}

// update the cart in user object (for logged user and logged user in registred users)
function updateUserCart(): void {
    let index = getLoggedUserIndex(JSON.parse(localStorage.getItem("logged_user")));
    users[index].cart = [];
    for (let item of orderedCart) {
        users[index].cart.push(item);
    }
    localStorage.setItem("logged_user", JSON.stringify(users[index]));
    localStorage.setItem("registered_users", JSON.stringify(users));
}

// place order for particular user and store in local storage
cartPlaceOrderButton.addEventListener("click", function () {
    let index = getLoggedUserIndex(JSON.parse(localStorage.getItem("logged_user")));
    for (let order of orderedCart) {
        users[index].orders.push(order);
    }

    // clear cart(in storage) when order placed
    users[index].cart = [];

    localStorage.setItem("logged_user", JSON.stringify(users[index]));
    localStorage.setItem("registered_users", JSON.stringify(users));
    // clear cart when order placed
    orderedCart.splice(0, orderedCart.length);
    orderLayoutInflator();
    display(ORDERS);
    alert("Order Successfully Placed");
});

function getLoggedUserIndex(logged_user: User) {
    for (let user of users) {
        if (user.mobile == logged_user.mobile) {
            return users.indexOf(user);
        }
    }
}