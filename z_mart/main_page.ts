// layouts/sections
let mainFrame: HTMLElement = document.getElementById("frame");

let productDisplayLayout: HTMLElement = document.getElementById("product_display");
let product_info_layout: HTMLElement = document.getElementById("product_info");
let cartLayout :HTMLElement = document.getElementById("cart_layout");
let productListLayout: HTMLElement = document.getElementById("product_list_layout");
let ordersLayout: HTMLElement = document.getElementById("order_layout");

let loginName:HTMLParagraphElement = document.getElementById("loginName") as HTMLParagraphElement;
let cartButton:HTMLParagraphElement = document.getElementById("cartButton") as HTMLParagraphElement;
let ordersButton:HTMLParagraphElement = document.getElementById("ordersButton") as HTMLParagraphElement;
let login:HTMLParagraphElement =document.getElementById("login") as HTMLParagraphElement;
// all products
let backup:any ; 

// var response:any;
display(PRODUCT_DISPLAY);
getContent(url);
refreshLoad();  
changeLoginState();
loadProducts(productListResponse);

function loadProducts(productList:Array<Product>): void
{
    display(PRODUCT_DISPLAY);
    emptyProductDisplay();
    console.log(productList);
    if(productList != null && productList.length>0)
    {
        console.log("here");
        // run loop to append the products in the display
        for(var item of productList)
        {
            createCard(item);
        }
    }
    else
    {
        //getContent(url);
        refreshLoad();
        //alert("No Products to Display");
    }
}