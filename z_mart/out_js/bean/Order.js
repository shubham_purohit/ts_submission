class Order {
    constructor(product, productID, quantity, totalPrice) {
        this.product = product;
        this.productID = productID;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
    }
}
//# sourceMappingURL=Order.js.map