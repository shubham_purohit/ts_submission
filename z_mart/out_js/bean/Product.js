class Product {
    constructor(id, name, category, gender, color, size, price, stock, material, brand, description, image) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.gender = gender;
        this.color = color;
        this.size = size;
        this.price = price;
        this.material = material;
        this.brand = brand;
        this.description = description;
        this.image = image;
    }
}
//# sourceMappingURL=Product.js.map