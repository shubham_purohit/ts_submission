class User {
    constructor(f_name, s_name, mobile, password, orders, cart) {
        this.f_name = f_name;
        this.s_name = s_name;
        this.mobile = mobile;
        this.password = password;
        this.orders = orders;
        this.cart = cart;
    }
}
//# sourceMappingURL=User.js.map