let cartListLayout = document.getElementById("cart_list_div");
let totalQuantityDis = document.getElementById("total_quantity");
let totalPriceDis = document.getElementById("total_price");
let cartItemHeading = document.createElement("div");
cartItemHeading.innerHTML = "Cart Items";
cartItemHeading.setAttribute("class", "cart_heading");
let cartPlaceOrderButton = document.createElement("h1");
cartPlaceOrderButton.setAttribute("class", "place_order_button");
cartPlaceOrderButton.innerHTML = "Place Order";
// global items in cart variabl
let orderedCart = JSON.parse(localStorage.getItem("logged_user")).cart;
let users = JSON.parse(localStorage.getItem("registered_users"));
let actualPrice;
let quantity;
let totalPrice = 0;
let totalQuantity = 0;
function cartLayoutInflator() {
    updateUserCart();
    emptyCartDisplay();
    updateTotalPrice();
    if (orderedCart.length > 0) {
        cartListLayout.style.backgroundImage = "none";
        cartListLayout.appendChild(cartItemHeading);
        for (var order of orderedCart) {
            createCartCard(order);
        }
    }
    else {
        emptyCartDisplay();
        cartListLayout.style.backgroundImage = "url('images/empty_cart.png')";
    }
    if (orderedCart.length > 0) {
        cartListLayout.appendChild(cartPlaceOrderButton);
    }
}
// card for each product in the cart
function createCartCard(order) {
    let cartCard = document.createElement("div");
    let infoContainer = document.createElement("div");
    let imageContainer = document.createElement("div");
    let cartImage = document.createElement("img");
    let cartName = document.createElement("h4");
    let cartPrice = document.createElement("p");
    let cartSize = document.createElement("p");
    let quantityDiv = document.createElement("div");
    let cartQuantity = document.createElement("input");
    let add_button = document.createElement("img");
    let minus_button = document.createElement("img");
    let cartItemDelete = document.createElement("p");
    cartCard.setAttribute("id", "cart_card");
    imageContainer.setAttribute("id", "image_container");
    infoContainer.setAttribute("id", "info_container");
    add_button.setAttribute("class", "add");
    minus_button.setAttribute("class", "minus");
    cartImage.src = order.product.image;
    cartName.innerHTML = order.product.name;
    cartPrice.innerHTML = "Special Price: Rs: " + order.totalPrice + "/-";
    cartSize.innerHTML = "Size: " + order.product.size;
    cartQuantity.innerHTML = "Qnty.";
    cartQuantity.value = order.quantity;
    cartQuantity.setAttribute("type", "number");
    cartQuantity.setAttribute("productID", order.product.id);
    cartQuantity.addEventListener("change", function (event) {
        handleQuantity(add_button, event.currentTarget);
    });
    add_button.src = "images/add_icon.png";
    add_button.addEventListener("click", function (event) {
        handleQuantity(event.currentTarget, cartQuantity);
    });
    minus_button.addEventListener("click", function (event) {
        handleQuantity(event.currentTarget, cartQuantity);
    });
    minus_button.src = "images/minus_icon.png";
    minus_button.style.width = "22px";
    minus_button.style.height = "22px";
    quantityDiv.appendChild(add_button);
    quantityDiv.append(cartQuantity);
    quantityDiv.appendChild(minus_button);
    cartItemDelete.innerHTML = "REMOVE";
    cartItemDelete.setAttribute("class", "remove_cart_item");
    cartItemDelete.setAttribute("productId", order.productID);
    cartItemDelete.addEventListener('click', function (event) {
        deleteCartItem(event.currentTarget);
    });
    infoContainer.appendChild(cartName);
    infoContainer.appendChild(cartPrice);
    infoContainer.appendChild(cartSize);
    // infoContainer.appendChild(minus_button);
    // infoContainer.appendChild(cartQuantity);
    // infoContainer.appendChild(add_button);
    infoContainer.appendChild(quantityDiv);
    infoContainer.appendChild(cartItemDelete);
    imageContainer.appendChild(cartImage);
    cartCard.appendChild(imageContainer);
    cartCard.appendChild(infoContainer);
    cartListLayout.appendChild(cartCard);
}
// handle the quantity change
function handleQuantity(buttonElement, inputElement) {
    // avoid negative entry
    if (buttonElement.getAttribute("class") == "add") {
        inputElement.value = (inputElement.value * 1 + 1);
    }
    else {
        inputElement.value = (inputElement.value * 1 - 1);
    }
    if (inputElement.value < 1) {
        inputElement.value = "1";
    }
    updateIndividualPrice(inputElement);
    // updateTotalPrice();
    cartLayoutInflator();
}
// delete cart item
function deleteCartItem(element) {
    let cartItem;
    // fetch the clicked item
    cartItem = getCartProduct(orderedCart, element.getAttribute("productID"));
    let index = orderedCart.indexOf(cartItem);
    orderedCart.splice(index, 1);
    cartLayoutInflator();
}
// update the item price after quantity change
function updateIndividualPrice(element) {
    let cartItem;
    // fetch the clicked item
    cartItem = getCartProduct(orderedCart, element.getAttribute("productID"));
    let index = orderedCart.indexOf(cartItem);
    actualPrice = cartItem.product.price;
    orderedCart[index].quantity = element.value;
    quantity = orderedCart[index].quantity;
    orderedCart[index].totalPrice = actualPrice * quantity;
}
// update total price after quantity change
function updateTotalPrice() {
    totalQuantity = totalPrice = 0;
    totalPriceDis.innerHTML = " ";
    totalQuantityDis.innerHTML = " ";
    for (var cartItem of orderedCart) {
        totalPrice = (totalPrice + (1 * cartItem.totalPrice));
        totalQuantity = (totalQuantity + (1 * cartItem.quantity));
        console.log(totalPrice);
        console.log(totalQuantity);
    }
    console.log(totalPrice);
    console.log(totalQuantity);
    totalQuantityDis.innerHTML = "Total Quantity:  " + totalQuantity;
    totalPriceDis.innerHTML = "Total Price:  Rs " + totalPrice + "/-";
}
// fetch the  product from the cart
function getCartProduct(orderCart, productID) {
    for (var order of orderCart) {
        if (order.productID == productID) {
            return order;
        }
    }
    return null;
}
// refresh the cart items list display
function emptyCartDisplay() {
    while (cartListLayout.firstChild) {
        cartListLayout.removeChild(cartListLayout.firstChild);
    }
}
// id already in cart or not
function inCart(cartOrder, order) {
    for (var item of cartOrder) {
        if (item.productID == order.productID)
            return true;
    }
    return false;
}
// update the cart in user object (for logged user and logged user in registred users)
function updateUserCart() {
    let index = getLoggedUserIndex(JSON.parse(localStorage.getItem("logged_user")));
    users[index].cart = [];
    for (let item of orderedCart) {
        users[index].cart.push(item);
    }
    localStorage.setItem("logged_user", JSON.stringify(users[index]));
    localStorage.setItem("registered_users", JSON.stringify(users));
}
// place order for particular user and store in local storage
cartPlaceOrderButton.addEventListener("click", function () {
    let index = getLoggedUserIndex(JSON.parse(localStorage.getItem("logged_user")));
    for (let order of orderedCart) {
        users[index].orders.push(order);
    }
    // clear cart(in storage) when order placed
    users[index].cart = [];
    localStorage.setItem("logged_user", JSON.stringify(users[index]));
    localStorage.setItem("registered_users", JSON.stringify(users));
    // clear cart when order placed
    orderedCart.splice(0, orderedCart.length);
    orderLayoutInflator();
    display(ORDERS);
    alert("Order Successfully Placed");
});
function getLoggedUserIndex(logged_user) {
    for (let user of users) {
        if (user.mobile == logged_user.mobile) {
            return users.indexOf(user);
        }
    }
}
//# sourceMappingURL=cart_page.js.map