//----- filer options section-----
var clear_filter = document.getElementById("clear_button");
// final response array from filters
var box_filtered_products = [];
function filter_handler() {
    // fetch all the checkboxes clicked
    var colors_checked = document.querySelectorAll('input[class="color"]:checked');
    var materials_checked = document.querySelectorAll('input[class="material"]:checked');
    var brands_checked = document.querySelectorAll('input[class="brand"]:checked');
    // empty for each new combination of filters
    box_filtered_products.splice(0, box_filtered_products.length);
    if (colors_checked.length || materials_checked.length || brands_checked.length)
        filters_enabled = true;
    else
        filters_enabled = false;
    var colors = [];
    var materials = [];
    var brands = [];
    // -------- fetch  checked boxs value---------
    for (var color of colors_checked) {
        colors.push(color.getAttribute("value"));
    }
    for (var material of materials_checked) {
        materials.push(material.getAttribute("value"));
    }
    for (var brand of brands_checked) {
        brands.push(brand.getAttribute("value"));
    }
    // color based updation
    if (colors.length) {
        var result = get_products_by_color(colors);
        // check if product already not present and update box_filtered_products
        for (var object of result) {
            if (!in_array(object, box_filtered_products)) {
                box_filtered_products.push(object);
            }
        }
    }
    // material based updation
    if (materials.length) {
        var result = get_products_by_material(materials);
        // check if product already not present and update box_filtered_products
        for (var object of result) {
            if (!in_array(object, box_filtered_products)) {
                box_filtered_products.push(object);
            }
        }
    }
    // brands based updation
    if (brands.length) {
        var result = get_products_by_brand(brands);
        // check if product already not present and update box_filtered_products
        for (var object of result) {
            if (!in_array(object, box_filtered_products)) {
                box_filtered_products.push(object);
            }
        }
    }
    // // if no selection
    // if (colors.length == 0 && materials.length == 0 || brands.length == 0) {
    //     box_filtered_products.splice(0, box_filtered_products.length);
    // }
    // if no selection
    if (!filters_enabled) {
        box_filtered_products.splice(0, box_filtered_products.length);
    }
    // display final filetered products
    if (box_filtered_products.length > 0) {
        load_products(box_filtered_products);
    }
    else {
        if (filters_enabled) {
            alert("Nothing matches your selection");
        }
        load_products(tab_filtered_products);
    }
}
function get_products_by_color(colors) {
    var response = [];
    for (var product of tab_filtered_products) {
        if (colors.indexOf(product.color) != -1) {
            response.push(product);
        }
    }
    return response;
}
function get_products_by_material(materials) {
    var response = [];
    for (var product of tab_filtered_products) {
        if (materials.indexOf(product.material) != -1) {
            response.push(product);
        }
    }
    return response;
}
function get_products_by_brand(brands) {
    var response = [];
    for (var product of tab_filtered_products) {
        if (brands.indexOf(product.brand) != -1) {
            response.push(product);
        }
    }
    return response;
}
clear_filter.addEventListener("click", function () {
    clearAllFilters();
    filter_handler();
});
function clearAllFilters() {
    var checkboxes = document.getElementsByTagName("input");
    for (var checkbox of checkboxes) {
        if (checkbox.checked)
            checkbox.checked = false;
    }
}
//# sourceMappingURL=filterHandler.js.map