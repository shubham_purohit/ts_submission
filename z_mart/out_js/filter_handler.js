//----- filer options section-----
let clearFilter = document.getElementById("clear_button");
// final response array from filters
let boxFilteredProducts = [];
let combinedFilteredProducts = [];
let colorsChecked;
let materialsChecked;
let brandsChecked;
let colors = [];
let materials = [];
let brands = [];
function filterHandler() {
    // fetch all the checkboxes clicked
    colorsChecked = document.querySelectorAll('input[class="color"]:checked');
    materialsChecked = document.querySelectorAll('input[class="material"]:checked');
    brandsChecked = document.querySelectorAll('input[class="brand"]:checked');
    // empty for each new combination of filters
    boxFilteredProducts.splice(0, boxFilteredProducts.length);
    combinedFilteredProducts.splice(0, combinedFilteredProducts.length);
    if (colorsChecked.length || materialsChecked.length || brandsChecked.length)
        filtersEnabled = true;
    else
        filtersEnabled = false;
    colors = [];
    materials = [];
    brands = [];
    // -------- fetch  checked boxs value---------
    for (var color of colorsChecked) {
        colors.push(color.getAttribute("value"));
    }
    for (var material of materialsChecked) {
        materials.push(material.getAttribute("value"));
    }
    for (var brand of brandsChecked) {
        brands.push(brand.getAttribute("value"));
    }
    // if no selection
    if (!filtersEnabled) {
        boxFilteredProducts.splice(0, boxFilteredProducts.length);
        combinedFilteredProducts.splice(0, combinedFilteredProducts.length);
    }
    else {
        filterResult();
    }
    if (combinedFilteredProducts.length) {
        loadProducts(combinedFilteredProducts);
        productListLayout.style.backgroundImage = "none";
    }
    else {
        if (filtersEnabled) {
            emptyProductDisplay();
            productListLayout.style.backgroundImage = "url('images/no_data.png')";
            alert("Nothing matches your selection");
        }
        else {
            productListLayout.style.backgroundImage = "none";
            loadProducts(tabFilteredProducts);
        }
    }
}
function filterResult() {
    for (let item of tabFilteredProducts) {
        if (colors.length) {
            if (brands.length && materials.length) {
                if (inArray(item.color, colors) && inArray(item.material, materials)
                    && inArray(item.brand, brands)) {
                    combinedFilteredProducts.push(item);
                }
            }
            else if (brands.length && !materials.length) {
                if (inArray(item.color, colors) && inArray(item.brand, brands)) {
                    combinedFilteredProducts.push(item);
                }
            }
            else if (!brands.length && materials.length) {
                if (inArray(item.color, colors) && inArray(item.material, materials)) {
                    combinedFilteredProducts.push(item);
                }
            }
            else if (!brands.length && !materials.length) {
                if (inArray(item.color, colors)) {
                    combinedFilteredProducts.push(item);
                }
            }
        }
        else if (!colors.length) {
            if (brands.length && materials.length) {
                if (inArray(item.brand, brands) && inArray(item.material, materials)) {
                    combinedFilteredProducts.push(item);
                }
            }
            else if (brands.length && !materials.length) {
                if (inArray(item.brand, brands)) {
                    combinedFilteredProducts.push(item);
                }
            }
            else if (!brands.length && materials.length) {
                if (inArray(item.material, materials)) {
                    combinedFilteredProducts.push(item);
                }
            }
        }
    }
}
clearFilter.addEventListener("click", function () {
    clearAllFilters();
    filterHandler();
});
function clearAllFilters() {
    var checkboxes = document.getElementsByTagName("input");
    for (var checkbox of checkboxes) {
        if (checkbox.checked)
            checkbox.checked = false;
    }
}
function inArray(item, array) {
    if (array.indexOf(item) != -1) {
        return true;
    }
    return false;
}
//# sourceMappingURL=filter_handler.js.map