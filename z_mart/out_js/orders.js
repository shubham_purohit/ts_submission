let orderListLayout = document.getElementById("order_list_div");
let orderItemHeading = document.createElement("div");
orderItemHeading.innerHTML = "Your Orders";
orderItemHeading.setAttribute("class", "order_heading");
function orderLayoutInflator() {
    let loggedUserOrders = JSON.parse(localStorage.getItem("logged_user")).orders;
    emptyOrderDisplay();
    orderListLayout.appendChild(orderItemHeading);
    if (loggedUserOrders.length === 0) {
        emptyOrderDisplay();
        orderListLayout.style.backgroundImage = "url('images/no_orders.png')";
    }
    for (var order of loggedUserOrders) {
        orderListLayout.style.backgroundImage = "white";
        createOrderCard(order);
    }
}
// card for each product in the cart
function createOrderCard(order) {
    let orderCard = document.createElement("div");
    let infoContainer = document.createElement("div");
    let imageContainer = document.createElement("div");
    let deliveryContainer = document.createElement("div");
    let orderImage = document.createElement("img");
    let orderName = document.createElement("h4");
    let orderPrice = document.createElement("p");
    let orderSize = document.createElement("p");
    let orderQuantity = document.createElement("p");
    orderCard.setAttribute("id", "order_card");
    imageContainer.setAttribute("id", "o_image_container");
    infoContainer.setAttribute("id", "o_info_container");
    deliveryContainer.setAttribute("id", "o_delivery_container");
    deliveryContainer.innerHTML = "Expect Deliveery in 5-7 Business Working Days";
    orderImage.src = order.product.image;
    orderName.innerHTML = order.product.name;
    orderPrice.innerHTML = "Price: Rs: " + order.totalPrice + "/-";
    orderSize.innerHTML = "Size: " + order.product.size;
    orderQuantity.innerHTML = "Quantity: " + order.quantity;
    orderQuantity.setAttribute("type", "number");
    orderQuantity.setAttribute("productID", order.product.id);
    // orderQuantity.addEventListener("change",function(event)
    // {
    //     handleQuantity(event.currentTarget as HTMLInputElement);
    // });
    orderPrice.style.fontSize = "larger";
    infoContainer.appendChild(orderName);
    infoContainer.appendChild(orderPrice);
    infoContainer.appendChild(orderSize);
    infoContainer.appendChild(orderQuantity);
    imageContainer.appendChild(orderImage);
    orderCard.appendChild(imageContainer);
    orderCard.appendChild(infoContainer);
    orderCard.appendChild(deliveryContainer);
    orderListLayout.appendChild(orderCard);
}
// refresh the order items list display
function emptyOrderDisplay() {
    while (orderListLayout.firstChild) {
        orderListLayout.removeChild(orderListLayout.firstChild);
    }
}
//# sourceMappingURL=orders.js.map