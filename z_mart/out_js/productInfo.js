var p_heading = document.getElementById("p_heading");
var p_image = document.getElementById("p_image");
var p_price = document.getElementById("p_price");
var p_stock = document.getElementById("p_stock");
var p_description = document.getElementById("p_description");
function productClickHandler(element) {
    alert(element.getAttribute("id"));
    display(product_info);
    // populate the page
    layoutInflater(getProduct(product_list_response, element.getAttribute("id")));
}
function layoutInflater(product) {
    p_heading.innerHTML = product.name;
    p_price.innerHTML = product.price;
    p_description.innerHTML = product.description;
    p_image.setAttribute("src", product.image);
    p_image.style.width = "350px";
    p_image.style.height = "450px";
    if (product.stock < 5) {
        p_stock.innerHTML = "Hurry! Only Few Left";
        p_stock.style.color = "Red";
    }
    else if (product.stock == 0) {
        p_stock.innerHTML = "Currently Unavailable";
        p_stock.style.color = "Red";
    }
    else {
        p_stock.innerHTML = "In Stock";
        p_stock.style.color = "lightgreen";
    }
}
function getProduct(products, id) {
    // var product:JSON;
    for (var item of products) {
        if (item.id == id) {
            return item;
        }
    }
    return null;
}
//# sourceMappingURL=productInfo.js.map