let productHeading = document.getElementById("p_heading");
let productImage = document.getElementById("p_image");
let prodcutPrice = document.getElementById("p_price");
let productSize = document.getElementById("p_size");
let proeuctStock = document.getElementById("p_stock");
let p_description = document.getElementById("p_description");
let addToCart = document.getElementById("add_to_cart_button");
// interface keyable {
//     [key: string]: (string | number);  
// }
let selectedProductId;
function productClickHandler(element) {
    //alert(element.getAttribute("id"));
    selectedProductId = element.getAttribute("id");
    display(PRODUCT_INFO);
    // populate the page
    productInfoInflater(getProduct(productListResponse, selectedProductId));
}
function productInfoInflater(product) {
    productHeading.innerHTML = product.name;
    productSize.innerHTML = "Size: " + product.size;
    prodcutPrice.innerHTML = "Special Price: Rs " + product.price + "/-";
    p_description.innerHTML = "Description: " + product.description;
    productImage.setAttribute("src", product.image);
    productImage.style.width = "350px";
    productImage.style.height = "450px";
    if (product.stock < 5) {
        proeuctStock.innerHTML = "Hurry! Only " + product.stock + " Left";
        proeuctStock.style.color = "Red";
    }
    else if (product.stock == 0) {
        proeuctStock.innerHTML = "Currently Unavailable";
        proeuctStock.style.color = "Red";
    }
    else {
        proeuctStock.innerHTML = "In Stock";
        proeuctStock.style.color = "lightgreen";
    }
}
function getProduct(products, selectedProductId) {
    // var product:JSON;
    for (var item of products) {
        if (item.id == selectedProductId) {
            return item;
        }
    }
    return null;
}
addToCart.addEventListener("click", function () {
    tabCategory = "none";
    changeTabColor();
    if (JSON.parse(localStorage.getItem("logged_user")).f_name == "") {
        alert("Please Login To Proceed!");
        location.href = "sign_in.html";
    }
    else {
        let product = getProduct(productListResponse, selectedProductId);
        let order = new Order(product, product.id, 1, product.price);
        if (!inCart(orderedCart, order)) {
            orderedCart.push(order);
        }
        display(CART_INFO);
        cartListLayout.style.backgroundColor = "linen";
        cartLayoutInflator();
    }
});
//# sourceMappingURL=product_info.js.map