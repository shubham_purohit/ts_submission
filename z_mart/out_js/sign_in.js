let loginButton = document.getElementById("login_button");
let signUp = document.getElementById("sign_up_button");
let firstName = document.getElementById("login_f_name");
let mobileNo = document.getElementById("login_mobile_no");
let password = document.getElementById("login_password");
let loggedUser;
// fetch from the local storage
let response = localStorage.getItem("registered_users");
if (response == undefined || response == null) {
    var temp = [];
    // default empty array at localstorage
    localStorage.setItem("registered_users", JSON.stringify(temp));
}
// login button even handling
loginButton.addEventListener("click", function () {
    let response = JSON.parse(localStorage.getItem("registered_users"));
    console.log(response);
    if (response != undefined && response != null) {
        if (validateSingIn(response)) {
            localStorage.setItem("logged_user", JSON.stringify(loggedUser));
            location.replace("main_page.html");
        }
    }
});
// validation for user login 
function validateSingIn(response) {
    // empty fields length check
    if (!firstName.value || !mobileNo.value || !password.value) {
        alert("Fields Empty. All fields are mandatory");
        return false;
    }
    // mobile no length check
    if (mobileNo.value.length != 10) {
        alert("Mobile no should be 10 digit long");
        return false;
    }
    var index = userPresent(response);
    if (index == -1) {
        alert("No user with this mobile no!");
        return false;
    }
    // password check
    if (password.value != response[index].password || firstName.value != response[index].f_name) {
        alert("Enter Correct Credentials\nEither password or username is incorrect");
        return false;
    }
    else {
        // store the logged user on login in LS
        loggedUser = response[index];
    }
    return true;
}
// sign up button redirtecting
signUp.addEventListener("click", function () {
    location.replace("sign_up.html");
});
function userPresent(response) {
    // search for user mobile no
    for (var user of response) {
        console.log(user.mobile);
        if (mobileNo.value == user.mobile) {
            return response.indexOf(user);
        }
    }
    return -1;
}
//# sourceMappingURL=sign_in.js.map