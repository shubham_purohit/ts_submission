let submitButton = document.getElementById("submit_button");
let signIn = document.getElementById("sign_in_button");
let fstName = document.getElementById("first_name");
let sndName = document.getElementById("second_name");
let mobNo = document.getElementById("mobile_no");
let pass = document.getElementById("password");
let user;
// sign up validation
function validateSignUp(user_details) {
    if (!fstName.value || !sndName.value || !mobNo.value || !pass.value) {
        alert("Fields Empty. All fields are mandatory");
        return false;
    }
    for (var no of user_details) {
        if (user.mobile == no.mobile) {
            alert("User already present with the same mobile no");
            return false;
        }
    }
    if (user.mobile.length != 10) {
        alert("Mobile no should be 10 digit long");
        return false;
    }
    if (!(user.password.match(/[0-9]+/g))) {
        alert("Password must contain one number");
        return false;
    }
    return true;
}
submitButton.addEventListener("click", function () {
    // orders associated with the user
    let orders = [];
    let cart = [];
    // new object from the details entered
    user = new User(fstName.value, sndName.value, mobNo.value, pass.value, orders, cart);
    console.log(user);
    let response = localStorage.getItem("registered_users");
    let UserDetails = JSON.parse(response);
    console.log(UserDetails);
    if (UserDetails != undefined && UserDetails != null) {
        if (validateSignUp(UserDetails)) {
            UserDetails.push(user);
            localStorage.setItem("registered_users", JSON.stringify(UserDetails));
            alert("User added succesfully");
            location.replace("sign_in.html");
        }
    }
});
signIn.addEventListener("click", function () {
    location.replace("sign_in.html");
});
//# sourceMappingURL=sign_up.js.map