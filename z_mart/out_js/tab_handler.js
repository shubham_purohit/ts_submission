let mensTab = document.getElementById("mens_tab");
let womensTab = document.getElementById("womens_tab");
let kidsTab = document.getElementById("kids_tab");
let homeTab = document.getElementById("home_tab");
let logoBanner = document.getElementById("logo_banner");
// default products list
// let tab_filtered_products: any = productListResponse;
let tabFilteredProducts = productListResponse;
// main page 
homeTab.addEventListener("click", function () {
    tabCategory = "home";
    tabFilteredProducts = productListResponse;
    changeTabColor();
    filterHandler();
    //load_products(product_list_response);
    refreshLoad();
});
// mens tab
mensTab.addEventListener("click", function () {
    tabCategory = "men";
    console.log(productListResponse);
    tabFilteredProducts = productListResponse;
    changeTabColor();
    //tabFilteredProducts.splice(0, tabFilteredProducts.length);
    tabFilteredProducts = getListByTab(productListResponse);
    filterHandler();
    //load_products(tab_filtered_products);
});
//womens tab
womensTab.addEventListener("click", function () {
    tabCategory = "women";
    tabFilteredProducts = productListResponse;
    changeTabColor();
    //tabFilteredProducts.splice(0, tabFilteredProducts.length);
    tabFilteredProducts = getListByTab(productListResponse);
    filterHandler();
    //load_products(tab_filtered_products);
});
// kids tab
kidsTab.addEventListener("click", function () {
    tabCategory = "kids";
    tabFilteredProducts = productListResponse;
    changeTabColor();
    //tabFilteredProducts.splice(0, tabFilteredProducts.length);
    console.log(tabFilteredProducts);
    tabFilteredProducts = getListByTab(productListResponse);
    filterHandler();
    //load_products(tab_filtered_products);;
});
// get the product list by tab selection
function getListByTab(products) {
    var response = [];
    for (var item of products) {
        if (item.gender == tabCategory) {
            response.push(item);
        }
    }
    return response;
}
login.addEventListener("click", function () {
    if (JSON.parse(localStorage.getItem("logged_user")).f_name == "") {
        location.href = "sign_in.html";
    }
    else {
        // clear cart on logout
        orderedCart.splice(0, orderedCart.length);
        changeLoginState();
        alert("Successfully Logged Out");
    }
});
cartButton.addEventListener('click', function () {
    tabCategory = "none";
    changeTabColor();
    if (JSON.parse(localStorage.getItem("logged_user")).f_name == "") {
        alert("Please Login To Proceed!");
        location.href = "sign_in.html";
    }
    else {
        display(CART_INFO);
        cartLayoutInflator();
    }
});
ordersButton.addEventListener('click', function () {
    tabCategory = "none";
    changeTabColor();
    if (JSON.parse(localStorage.getItem("logged_user")).f_name == "") {
        alert("Please Login To Proceed!");
        location.href = "sign_in.html";
    }
    else {
        display(ORDERS);
        orderLayoutInflator();
    }
});
//# sourceMappingURL=tab_handler.js.map