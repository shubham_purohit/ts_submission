let productHeading:HTMLElement = <HTMLElement> document.getElementById("p_heading");
let productImage:HTMLElement = <HTMLElement> document.getElementById("p_image");
let prodcutPrice:HTMLElement = <HTMLElement> document.getElementById("p_price");
let productSize:HTMLElement = <HTMLElement> document.getElementById("p_size")
let proeuctStock:HTMLElement = <HTMLElement>document.getElementById("p_stock");
let p_description:HTMLElement = <HTMLElement>document.getElementById("p_description");

let addToCart: HTMLElement = <HTMLElement>document.getElementById("add_to_cart_button");

// interface keyable {
//     [key: string]: (string | number);  
// }

let selectedProductId:string;

function productClickHandler(element: HTMLElement): void {
    //alert(element.getAttribute("id"));
    selectedProductId = element.getAttribute("id")
    display(PRODUCT_INFO);
    // populate the page
    productInfoInflater(getProduct(productListResponse,selectedProductId));
}

function productInfoInflater(product:Product):void {
    productHeading.innerHTML = product.name;
    productSize.innerHTML = "Size: " + product.size;
    prodcutPrice.innerHTML  = "Special Price: Rs " + product.price + "/-";
    p_description.innerHTML = "Description: " + product.description;
    productImage.setAttribute("src",product.image);
    productImage.style.width="350px";
    productImage.style.height="450px";

    if(product.stock < 5){
        proeuctStock.innerHTML = "Hurry! Only " + product.stock + " Left";
        proeuctStock.style.color = "Red";
    }
    else if(product.stock == 0){
        proeuctStock.innerHTML = "Currently Unavailable";
        proeuctStock.style.color = "Red";
    }
    else{
        proeuctStock.innerHTML = "In Stock";
        proeuctStock.style.color = "lightgreen";
    }
}

function getProduct(products:Array<Product>,selectedProductId:string):Product {
    // var product:JSON;
    for(var item of products)
    {
        if(item.id == selectedProductId)
        {
            return item;
        }
    }
    return null; 
}

addToCart.addEventListener("click",function() {
    tabCategory="none";
    changeTabColor();
    if(JSON.parse(localStorage.getItem("logged_user")).f_name == ""){
        alert("Please Login To Proceed!");
        location.href = "sign_in.html"; 
    }
    else{
        let product:Product = getProduct(productListResponse,selectedProductId);
        let order = new Order(product,product.id,1,product.price);
        if(!inCart(orderedCart,order))
        {
            orderedCart.push(order);
        }
        display(CART_INFO);
        cartListLayout.style.backgroundColor = "linen";
        cartLayoutInflator();
    }
});
