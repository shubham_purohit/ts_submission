let searchBox: HTMLInputElement = document.getElementById("search_box") as HTMLInputElement;

let searchedProductList: Array<Product> = [];

searchBox.addEventListener("keydown", function () {
    searchedProductList.splice(0, searchedProductList.length);
    searchHandler();
});

function searchHandler(): void {
    if (searchBox.value != "") {
        for (let product of productListResponse) {
            if(product.name.toLowerCase().search(searchBox.value.toLowerCase()) != -1)
            {
                if(!inFilteredArray(product,searchedProductList)){
                    searchedProductList.push(product);}     
            }
            if(product.category.toLowerCase().search(searchBox.value.toLowerCase()) != -1)
            {
                if(!inFilteredArray(product,searchedProductList)){
                    searchedProductList.push(product);}     
            }
            if(product.gender.toLowerCase().search(searchBox.value.toLowerCase()) != -1)
            {
                if(!inFilteredArray(product,searchedProductList)){
                    searchedProductList.push(product);}     
            }
            if(product.description.toLowerCase().search(searchBox.value.toLowerCase()) != -1)
            {
                if(!inFilteredArray(product,searchedProductList)){
                    searchedProductList.push(product);}     
            }
            if(product.material.toLowerCase().search(searchBox.value.toLowerCase()) != -1)
            {
                if(!inFilteredArray(product,searchedProductList)){
                    searchedProductList.push(product);}     
            }
            if(product.brand.toLowerCase().search(searchBox.value.toLowerCase()) != -1)
            {
                if(!inFilteredArray(product,searchedProductList)){
                    searchedProductList.push(product);}     
            }
        }
    }

    if(searchedProductList.length){
        productListLayout.style.backgroundImage = "none";
        loadProducts(searchedProductList);
    }
    else if(searchBox.value != ""){
        emptyProductDisplay();
        productListLayout.style.backgroundImage = "url('images/no_data.png')";
    }
    else{
        loadProducts(productListResponse);
    }
}



