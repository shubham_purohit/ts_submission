let submitButton:HTMLElement= document.getElementById("submit_button") as HTMLElement;
let signIn: HTMLElement = document.getElementById("sign_in_button") as HTMLElement;


let fstName: HTMLInputElement = document.getElementById("first_name") as HTMLInputElement;
let sndName: HTMLInputElement = document.getElementById("second_name") as HTMLInputElement;
let mobNo : HTMLInputElement= document.getElementById("mobile_no") as HTMLInputElement;
let pass: HTMLInputElement = document.getElementById("password") as HTMLInputElement;

let user:User;

// sign up validation
function validateSignUp(user_details:Array<User>) :boolean {

    if (!fstName.value || !sndName.value || !mobNo.value || !pass.value) {
        alert("Fields Empty. All fields are mandatory");
        return false;
    }

    for (var no of user_details) {
        if (user.mobile == no.mobile) {
            alert("User already present with the same mobile no");
            return false;
        }
    }

    if (user.mobile.length != 10) {
        alert("Mobile no should be 10 digit long");
        return false;
    }

    if (!(user.password.match(/[0-9]+/g))) {
        alert("Password must contain one number")
        return false;
    }

    return true;
}

submitButton.addEventListener("click", function():void {

    // orders associated with the user
    let orders:Array<Order> = []; 
    let cart:Array<Order> = [];

    // new object from the details entered
    user = new User(fstName.value,sndName.value,mobNo.value,pass.value,orders,cart);

    console.log(user);

    let response = localStorage.getItem("registered_users");
    let UserDetails:Array<User> = JSON.parse(response);

    console.log(UserDetails);

    if (UserDetails != undefined && UserDetails != null) {
        if (validateSignUp(UserDetails)) {
            UserDetails.push(user);
            localStorage.setItem("registered_users", JSON.stringify(UserDetails))
            alert("User added succesfully");
            location.replace("sign_in.html")
        }
    }
});

signIn.addEventListener("click", function() {
    location.replace("sign_in.html")
})