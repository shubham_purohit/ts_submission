let productListResponse: Array<Product> = [];

// display toggle
function display(mode: number): void {
    switch (mode) {
        case 1:
            productDisplayLayout.style.display = "block";
            product_info_layout.style.display = "none";
            cartLayout.style.display = "none";
            ordersLayout.style.display = "none";
            break;
        case 2:
            productDisplayLayout.style.display = "none";
            product_info_layout.style.display = "block";
            cartLayout.style.display = "none";
            ordersLayout.style.display = "none";
            break;
        case 3:
            productDisplayLayout.style.display = "none";
            product_info_layout.style.display = "none";
            cartLayout.style.display = "block";
            ordersLayout.style.display = "none";
            break;
        case 4:
            productDisplayLayout.style.display = "none";
            product_info_layout.style.display = "none";
            cartLayout.style.display = "none";
            ordersLayout.style.display = "block";
            break;
    }
}

function emptyProductDisplay(): void {
    while (productListLayout.firstChild) {
        productListLayout.removeChild(productListLayout.firstChild);
    }
}

function createCard(item: any): void {
    let card = document.createElement("div");
    card.setAttribute("id", item.id);
    card.style.width = "fit-content";
    card.style.margin = "20px";
    card.style.display = "inline-block";
    card.style.setProperty("border-radius", "5px");
    card.style.border = "solid";
    card.style.borderColor = "lightgray";
    card.style.borderWidth = "2px"
    card.style.setProperty("box-shadow", "0 4px 8px 0 rgba(0, 0, 0, 0.2)");
    card.onclick = function (event) {
        productClickHandler(event.currentTarget as HTMLElement);
    }
    card.onmouseenter = function () {
        card.style.setProperty("box-shadow", "0 10px 20px 0 rgba(0, 0, 0, 0.2)");
    }
    card.onmouseleave = function () {
        card.style.setProperty("box-shadow", "0 4px 8px 0 rgba(0, 0, 0, 0.2)");
    }

    let product_image = new Image;
    product_image.style.width = "200px"
    product_image.style.height = "280px"
    product_image.src = item.image;

    let container = document.createElement("div");
    let h_name = document.createElement("h4");
    let p_price = document.createElement("p");
    p_price.style.color = "orange";
    p_price.style.fontWeight = "bolder";
    p_price.style.fontFamily = "-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
    let p_brand = document.createElement("p");
    let p_material = document.createElement("p");

    container.style.textAlign = "center";
    container.style.padding = "2px 16px";
    container.style.width = "167px"
    container.style.whiteSpace = "normal"
    container.style.overflow = "clip";
    container.style.backgroundColor = "white";

    h_name.innerHTML = item.name;
    h_name.style.width = "100%"
    h_name.style.fontWeight = "bold";
    p_price.innerHTML = item.price + "/-";
    p_brand.innerHTML = item.brand;
    p_material.innerHTML = item.material;

    // // edit button on each card
    // var image_edit = new Image;
    // // handler for edit button 
    // image_edit.addEventListener("click", function() {
    //     //edit_handler(item);
    // });
    // image_edit.style.width = "20px"
    // image_edit.style.height = "20px"
    // image_edit.src = "images/edit_icon.png";

    container.appendChild(h_name);
    container.appendChild(p_price);
    container.appendChild(p_brand);
    container.appendChild(p_material);
    // container.appendChild(image_edit)
    // container.appendChild(image_delete)

    card.appendChild(product_image);
    card.appendChild(container)

    productListLayout.appendChild(card);
    productListLayout.style.height = "500px";
    productListLayout.style.overflow = "scroll";
}

// refresh loading the products from local storage
function refreshLoad(): void {
    // loadProducts(productListResponse);
    
    productListResponse = backup;
    tabFilteredProducts = productListResponse;
    //parseProductsData(backup);

    if(localStorage.getItem("logged_user") == undefined){
        localStorage.setItem("logged_user",JSON.stringify({"f_name":""}));
    }
    // if (product_list_response == undefined || product_list_response == null) {
    //     var product_list_response = [];
    //     localStorage.setItem("products_list", JSON.stringify(product_list_response));
    // }
}

// get the JSON data from the local file via local server
async function getContent(url: string): Promise<void> {
    try {
        const response = await fetch(url);
        const data: any = await response.json();
        console.log(data);
        backup = data;
        //localStorage.setItem('products', JSON.stringify(data));
        refreshLoad();
        // parseProductsData(backup);
        loadProducts(productListResponse);
    } catch (error) {
        console.log('Request failed', error);
    }
}

// let productsList: Array<Product> = [];

// function parseProductsData(responseData:any):void
// {
//     console.log(productListResponse);
    
//     for(var item of responseData)
//     {
//         let product = new Product(item.id,item.name,item.category,item.gender,
//             item.color,item.size,item.price,item.stock,item.material,item.brand,
//             item.description,item.image);
        
//             productListResponse.push(product);
//     }

//     console.log(productListResponse);
//     console.log(productListResponse);
// }

// handle table color base on selection
function changeTabColor(): void {
    if (tabCategory == "men") {
        homeTab.style.color = "black"
        mensTab.style.color = "white"
        womensTab.style.color = "black"
        kidsTab.style.color = "black";
    }
    else if (tabCategory == "women") {
        homeTab.style.color = "black"
        mensTab.style.color = "black"
        womensTab.style.color = "white"
        kidsTab.style.color = "black";
    }
    else if (tabCategory == "kids") {
        homeTab.style.color = "black"
        mensTab.style.color = "black"
        womensTab.style.color = "black"
        kidsTab.style.color = "white";
    }
    else if(tabCategory == "home"){
        homeTab.style.color = "white"
        mensTab.style.color = "black"
        womensTab.style.color = "black"
        kidsTab.style.color = "black";
    }
    else
    {
        homeTab.style.color = "black"
        mensTab.style.color = "black"
        womensTab.style.color = "black"
        kidsTab.style.color = "black"; 
    }
}

// changet the display of the login div 
function changeLoginState():void
{
    console.log(JSON.parse(localStorage.getItem("logged_user")).f_name);
    if (JSON.parse(localStorage.getItem("logged_user")).f_name == "") {
        loginName.style.display = "none";
        login.innerHTML = "Login";
        display(PRODUCT_DISPLAY);
    }
    else if(JSON.parse(localStorage.getItem("logged_user")).f_name && login.innerHTML == "Logout")
    {
        loginName.style.display = "none";
        login.innerHTML = "Login";
        localStorage.setItem("logged_user",JSON.stringify({"f_name":""}));
        display(PRODUCT_DISPLAY);
    }
    else {
        // logged in state
        loginName.style.display = "inline";
        loginName.innerHTML = "Welcome " + JSON.parse(localStorage.getItem("logged_user")).f_name;
        login.innerHTML = "Logout";
    }
}

function inFilteredArray(object: Product, array: Array<Product>): boolean {
    if (array.indexOf(object) != -1) {
        return true;
    }
    return false;
}